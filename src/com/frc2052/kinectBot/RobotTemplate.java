package com.frc2052.kinectBot;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.KinectStick;
import edu.wpi.first.wpilibj.SimpleRobot;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;

public class RobotTemplate extends SimpleRobot {

    private final KinectStick left = new KinectStick(1), right = new KinectStick(2);
    private final Solenoid outArm = new Solenoid(7), inArm = new Solenoid(8), flipIn = new Solenoid(1), flipOut = new Solenoid(2);
    private final Compressor compress = new Compressor(1, 1);
    private final double KINECT_STICK_TOGGLE = -1.0;
    private final Timer timer = new Timer();
    private boolean toggled = false;
    private double autoTime;
    private final ThrottledPrinter printArm = new ThrottledPrinter(1.0), printButtons = new ThrottledPrinter(1.0);

    public void autonomous() {
        retract();
        timer.stop();
        timer.reset();
        timer.start();
        compress.start();
        toggled = false;
        while (isAutonomous() && isEnabled()) {

            autoTime = timer.get();
            //Time Constaint is to simulate driving forward
            if (autoTime > 1.5 && !toggled) {
                //Get what side the robot is on 
                if (left.getRawButton(7)) {
                    if (isKinectLeftArmUp()) {
                        if (autoTime > 2.5 && autoTime < 3.7) {
                            extend();
                        }
                        if (autoTime > 3.5 && autoTime < 3.7) {
                            outward();
                        }
                    } else if (isKinectRightArmUp()) {
                        if (autoTime > 5 && autoTime < 6.3) {
                            extend();
                        }
                        if (autoTime > 6 && autoTime < 6.3) {
                            outward();
                        }
                    }
                }
                if (right.getRawButton(5)) {
                    if (isKinectLeftArmUp()) {
                        if (autoTime > 5 && autoTime < 6.3) {
                            extend();
                        }
                        if (autoTime > 6 && autoTime < 6.3) {
                            outward();
                        }
                    } else if (isKinectRightArmUp()) {
                        if (autoTime > 2.5 && autoTime < 3.7) {
                            extend();
                        }
                        if (autoTime > 3.5 && autoTime < 3.7) {
                            outward();
                        }
                    }
                }
            }
        }
    }

    public void operatorControl() {
        compress.start();
        retract();
        while (isOperatorControl() && isEnabled()) {
            printArm.println("Left:" + left.getY() + ", " + "Right:" + right.getY());
            printButtons.println("Head Left: " + left.getRawButton(2) + ", " + "Head Right: " + right.getRawButton(1));
        }
    }

    private boolean isKinectArmsUp() {
        return left.getY() == KINECT_STICK_TOGGLE && right.getY() == KINECT_STICK_TOGGLE;
    }

    private boolean isKinectLeftArmUp() {
        return left.getY() == KINECT_STICK_TOGGLE;
    }

    private boolean isKinectRightArmUp() {
        return right.getY() == KINECT_STICK_TOGGLE;
    }

    public void outward() {
        flipOut.set(true);
        flipIn.set(false);
    }

    public void inward() {
        flipOut.set(false);
        flipIn.set(true);
    }

    public void extend() {
        outArm.set(true);
        inArm.set(false);
    }

    public void retract() {
        inArm.set(true);
        outArm.set(false);

    }

}
